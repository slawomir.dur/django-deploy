import subprocess
import sys
import zipfile
import os
import json
import shutil
import requests
import colorama

def install(package):
  subprocess.check_call([sys.executable, "-m", "pip", "install", package])

colorama.init(convert=True, autoreset=True)

def print_colored(text, color=colorama.Fore.GREEN, has_white_line=True):
  if has_white_line:
    print()
  print('{}{}'.format(color,text), end='\n\n')

def print_header(header_text):
  print()
  print('========== {} =========='.format(header_text))
  print()

def print_dict(dictionary):
  for key, value in dictionary.items():
    print(key, end=': ')
    print_colored(value, colorama.Fore.MAGENTA, False)

def get_ini_param(param):
  ini_file_paht='ini.json'
  with open(ini_file_paht) as file:
    ini_file=json.load(file)
  try:
    ret_val=ini_file[param]
  except KeyError:
    raise
  return ret_val

def create_dir_if_not_exists(directory):
  if os.path.isdir(directory):
    print('Deleting '+directory+'...')
    shutil.rmtree(directory)
  if not os.path.isdir(directory):
    print_colored(directory+' not found.', colorama.Fore.YELLOW)
    os.mkdir(directory)
    print('Directory '+directory+' created.')

def delete_file_if_exists(file):
  if os.path.isdir(file):
    print_colored('File already {} exists.'.format(file), colorama.Fore.YELLOW)
    os.remove(file)
    print('File {} deleted.'.format(file))    
    
def str_to_bool(text):
  return True if text.strip().lower().capitalize() == 'True' else False

def ask_if_ok(text):
  print(text)
  ok=str_to_bool(input('Is it alright? [True/False]: '))
  if not ok:
    print('Exiting...')
    sys.exit()
  print('Continuing...')


try:
  SECRET_KEY_VAR=get_ini_param('SECRET_KEY_VARIABLE')
  if not SECRET_KEY_VAR in os.environ:
    raise RuntimeError('{} environment variable not found.'.format(SECRET_KEY_VAR))

  #get required parameters
  print_header('GETTING CONFIGURATION SETTINGS FROM INI.JSON')
  print('Getting required parameters...')
  DJANGO_PROJECT_NAME=get_ini_param('DJANGO_PROJECT_NAME')
  NGINX_DIRECTORY=get_ini_param('NGINX_DESTINATION_DIRECTORY')
  SYMPHONY_DIGGER_ZIP_PATH=get_ini_param('DJANGO_PROJECT_ZIP')
  SYMPHONY_DIGGER_DESTINATION_DIRECTORY=get_ini_param('DJANGO_PROJECT_DESTINATION_DIRECTORY')
  HOST_NAME=get_ini_param('HOST_NAME')
  HOST_IP=get_ini_param('HOST_IP')

  PROJECT_CONF_FILE_NAME="{}_nginx.conf".format(DJANGO_PROJECT_NAME)

  SETTINGS = {
    "ASK_ABOUT_CONFIG": str_to_bool(get_ini_param('ASK_ABOUT_CONFIG')),
    "IS_NGINX_INSTALLED": str_to_bool(get_ini_param('IS_NGINX_INSTALLED')),
    "DJANGO_PRO_NAME": get_ini_param('DJANGO_PROJECT_NAME'),
    "HOST_NAME": get_ini_param('HOST_NAME'),
    "HOST_IP": get_ini_param('HOST_IP')
  }

  print('Settings that will be used to configure the server.')
  print_dict(SETTINGS)

  if SETTINGS['ASK_ABOUT_CONFIG']:
    ask_if_ok('Please, check the settings.')

  print_header("SETTING UP ALL PATHS FOR INSTALLATION")
  PATHS = {
    "NGINX_INSTALLATION_URL": r'http://nginx.org/download/nginx-1.19.8.zip',
    "NGINX_ZIP": r"{}\{}".format(NGINX_DIRECTORY, 'nginx.zip'),
    "NGINX_DIRECTORY": NGINX_DIRECTORY,
    "DJANGO_PRO_ZIP": SYMPHONY_DIGGER_ZIP_PATH,
    "DJANGO_PRO_DEST": SYMPHONY_DIGGER_DESTINATION_DIRECTORY,
    "DJANGO_PRO_REQUIREMENTS" : r"{}\{}\{}".format(SYMPHONY_DIGGER_DESTINATION_DIRECTORY, DJANGO_PROJECT_NAME, 'requirements.txt'),
    "DJANGO_PRO_SETTINGS": r"{}\{}\{}\{}\{}".format(SYMPHONY_DIGGER_DESTINATION_DIRECTORY, DJANGO_PROJECT_NAME, DJANGO_PROJECT_NAME, DJANGO_PROJECT_NAME, 'settings.py'),
    "DJANGO_PRO_MANAGE": r"{}\{}\{}\{}".format(SYMPHONY_DIGGER_DESTINATION_DIRECTORY, DJANGO_PROJECT_NAME, DJANGO_PROJECT_NAME, 'manage.py'),
    "SITES_ENABLED_DIR": r"{}\{}\{}".format(NGINX_DIRECTORY, 'nginx-1.19.8', 'sites-enabled'),
    "SITES_AVAILABLE_DIR": r"{}\{}\{}".format(NGINX_DIRECTORY, 'nginx-1.19.8', 'sites-available'),
    "STATIC_DIR": r"{}\{}\{}\{}".format(SYMPHONY_DIGGER_DESTINATION_DIRECTORY, DJANGO_PROJECT_NAME, DJANGO_PROJECT_NAME, 'static'),
    "SITES_ENABLED_CONF": r"{}\{}".format("{}\\{}\\{}".format(NGINX_DIRECTORY, 'nginx-1.19.8', 'sites-enabled'), PROJECT_CONF_FILE_NAME),
    "SITES_AVAILABLE_CONF": r"{}\{}".format("{}\\{}\\{}".format(NGINX_DIRECTORY, 'nginx-1.19.8', 'sites-available'), PROJECT_CONF_FILE_NAME),
    "NGINX_CONF": r"{}\{}\{}\{}".format(NGINX_DIRECTORY, 'nginx-1.19.8', 'conf', 'nginx.conf'),
    "RUNSERVER_SCRIPT": r"{}\{}\{}\{}".format(SYMPHONY_DIGGER_DESTINATION_DIRECTORY, DJANGO_PROJECT_NAME, DJANGO_PROJECT_NAME, 'runserver.py')
  }
  print('Paths that will be used to configure the server.')
  print_dict(PATHS)

  if SETTINGS['ASK_ABOUT_CONFIG']:
    ask_if_ok('Please, check the paths.')

  if not SETTINGS['IS_NGINX_INSTALLED']:
    print()
    print_header("INSTALLING NGINX")

    #create directories if not exist
    create_dir_if_not_exists(PATHS['NGINX_DIRECTORY'])
    create_dir_if_not_exists(PATHS['DJANGO_PRO_DEST'])

    #download nginx
    print('Downloading nginx from '+PATHS['NGINX_INSTALLATION_URL']+'...')
    r=requests.get(PATHS['NGINX_INSTALLATION_URL'], allow_redirects=True)

    #save nginx to NGINX_DIRECTORY
    print('Saving downloaded file as '+PATHS['NGINX_ZIP']+'...')
    with open(PATHS['NGINX_ZIP'], 'wb+') as file:
      file.write(r.content)

    #unzip nginx
    print('Unzipping '+PATHS['NGINX_ZIP']+'...')
    with zipfile.ZipFile(PATHS['NGINX_ZIP'], 'r') as zip_ref:
      zip_ref.extractall(PATHS['NGINX_DIRECTORY'])

    print('Removing '+PATHS['NGINX_ZIP']+'...')
    os.remove(PATHS['NGINX_ZIP'])

    print_colored('NGINX successully installed.')

  print()
  print()
  print()

  print_header('INSTALLING DJANGO PROJECT')
  #unzip symphony_digger
  print('Extracting symphony_digger...')
  with zipfile.ZipFile(PATHS['DJANGO_PRO_ZIP'], 'r') as zip_ref:
    zip_ref.extractall(PATHS['DJANGO_PRO_DEST'])

  #install dependencies for symphony_digger
  print('Installing dependencies for symphony_digger...')

  subprocess.run(['pip','install','-r',PATHS['DJANGO_PRO_REQUIREMENTS']])
  print_colored('Dependencies installed.')

  print('Getting required packages names...')
  REQUIRED_PACKAGES=get_ini_param('REQUIRED_PACKAGES').split(',')
  print_colored('Required packages: {}'.format(REQUIRED_PACKAGES), colorama.Fore.MAGENTA)

  #installing other required packages
  print_colored('Installing other dependencies...')
  for PACKAGE in REQUIRED_PACKAGES:
    install(PACKAGE)

  print_colored('Packages successfully installed.')

  #change settings.py in symphony_digger
  print('Setting ALLOWED_HOSTS in '+PATHS['DJANGO_PRO_SETTINGS']+'...')

  #reading lines from settings.py
  with open(PATHS['DJANGO_PRO_SETTINGS'], 'r') as settings_file:
    settings_lines=settings_file.readlines()

  #find ALLOWED_HOSTS line
  for index, line in enumerate(settings_lines):
    if 'ALLOWED_HOSTS' in line:
      allowed_hosts_index=index

  #change ALLOWED_HOSTS line
  settings_lines[allowed_hosts_index]='ALLOWED_HOSTS = ["localhost","'+HOST_NAME+'","'+HOST_IP+'"]'

  #save settings.py file
  with open(PATHS['DJANGO_PRO_SETTINGS'], 'w') as settings_file:
    settings_file.writelines(settings_lines)
  print_colored('Settings file successfully updated.')

  #collect static files
  print('Collecting static files for symphony_digger...')
  subprocess.run(['python',PATHS['DJANGO_PRO_MANAGE'],'collectstatic'])
  print_colored('Collecting static files successful.')

  print_colored('Django project successfully installed.')

  print()
  print()
  print()
  print_header('CONFIGURING NGINX AND {}'.format(SETTINGS['DJANGO_PRO_NAME']))

  #create sites-enabled and sites-available
  print('Creating directories if not existent: {} {}...'.format(PATHS['SITES_ENABLED_DIR'], PATHS['SITES_AVAILABLE_DIR']))

  if not os.path.isdir(PATHS['SITES_ENABLED_DIR']):
    os.mkdir(PATHS['SITES_ENABLED_DIR'])
  if not os.path.isdir(PATHS['SITES_AVAILABLE_DIR']):
    os.mkdir(PATHS['SITES_AVAILABLE_DIR'])



  #create symphony_digger_nginx.conf
  SYM_DIG_CONF="""  
  # symphony_digger_nginx.conf

  # configuration of the server
  server {{
      # the port your site will be served on
      listen      80;
      # the domain name it will serve for
      server_name {}; # substitute your machine's IP address or FQDN
      charset     utf-8;

      # max upload size
      client_max_body_size 75M;   # adjust to taste

      # Django media
      location /media  {{
          alias /path/to/your/mysite/media;  # your Django project's media files - amend as required
      }}

      location /static {{
          alias {}; # your Django project's static files - amend as required
      }}

      # Finally, send all non-media requests to the Django server.
      location / {{
          proxy_pass http://localhost:8080; # See output from runserver.py
      }}
  }}""".format(HOST_NAME, PATHS['STATIC_DIR'].replace('\\','/'))

  print()
  print('Configuration file for the django project:')
  print_colored(SYM_DIG_CONF, colorama.Fore.BLUE)
  print()

  #save file conf files
  print('Creating config files for symphony_digger...')
  with open(PATHS['SITES_ENABLED_CONF'], 'w+') as sites_enabled_conf_file:
    sites_enabled_conf_file.write(SYM_DIG_CONF)
  with open(PATHS['SITES_AVAILABLE_CONF'], 'w+') as sites_available_conf_file:
    sites_available_conf_file.write(SYM_DIG_CONF)

  print()
  print_colored('Configuration file written to {} and {}'.format(PATHS['SITES_ENABLED_CONF'], PATHS['SITES_AVAILABLE_CONF']))
  print()

  #create nginx.conf file
  NGINX_CONF="""
  #user  nobody;
  worker_processes  1;

  #error_log  logs/error.log;
  #error_log  logs/error.log  notice;
  #error_log  logs/error.log  info;

  #pid        logs/nginx.pid;


  events {{
      worker_connections  1024;
  }}


  http {{
      include       mime.types;
      default_type  application/octet-stream;

      include       {};

      #log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
      #                  '$status $body_bytes_sent "$http_referer" '
      #                  '"$http_user_agent" "$http_x_forwarded_for"';

      #access_log  logs/access.log  main;

      sendfile        on;
      #tcp_nopush     on;

      #keepalive_timeout  0;
      keepalive_timeout  65;

      #gzip  on;

      server {{
          listen       10;
          server_name  localhost;

          #charset koi8-r;

          #access_log  logs/host.access.log  main;

          location / {{
              root   html;
              index  index.html index.htm;
          }}

          #error_page  404              /404.html;

          # redirect server error pages to the static page /50x.html
          #
          error_page   500 502 503 504  /50x.html;
          location = /50x.html {{
              root   html;
          }}

          # proxy the PHP scripts to Apache listening on 127.0.0.1:80
          #
          #location ~ \.php$ {{
          #    proxy_pass   http://127.0.0.1;
          #}}

          # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
          #
          #location ~ \.php$ {{
          #    root           html;
          #    fastcgi_pass   127.0.0.1:9000;
          #    fastcgi_index  index.php;
          #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
          #    include        fastcgi_params;
          #}}

          # deny access to .htaccess files, if Apache's document root
          # concurs with nginx's one
          #
          #location ~ /\.ht {{
          #    deny  all;
          #}}
      }}


      # another virtual host using mix of IP-, name-, and port-based configuration
      #
      #server {{
      #    listen       8000;
      #    listen       somename:8080;
      #    server_name  somename  alias  another.alias;

      #    location / {{
      #        root   html;
      #        index  index.html index.htm;
      #    }}
      #}}


      # HTTPS server
      #
      #server {{
      #    listen       443 ssl;
      #    server_name  localhost;

      #    ssl_certificate      cert.pem;
      #    ssl_certificate_key  cert.key;

      #    ssl_session_cache    shared:SSL:1m;
      #    ssl_session_timeout  5m;

      #    ssl_ciphers  HIGH:!aNULL:!MD5;
      #    ssl_prefer_server_ciphers  on;

      #    location / {{
      #        root   html;
      #        index  index.html index.htm;
      #    }}
      #}}

  }}
  """.format(PATHS['SITES_ENABLED_CONF'].replace('\\','/'))

  if SETTINGS['IS_NGINX_INSTALLED']:
    raise RuntimeError('Script does not support this option yet.')

  #check if conf file exists
  if not os.path.isfile(PATHS['NGINX_CONF']):
    raise RuntimeError('Could not find nginx.conf at {} - internal script error'.format(PATHS['NGINX_CONF']))

  #save conf
  print('Creating '+PATHS['NGINX_CONF']+'...')
  with open(PATHS['NGINX_CONF'], 'w') as nginx_conf_file:
    nginx_conf_file.write(NGINX_CONF)

  print_colored('Successfully adjusted nginx configuration file.')
  print()
  print()
  print('New contents of configuration file:', colorama.Fore.BLUE)
  print_colored(NGINX_CONF, colorama.Fore.BLUE)
  print()
  print()
  print()
  print_colored('Successfully configured nginx and django project.')

  NGINX_COMMAND=NGINX_DIRECTORY+r'\nginx.exe'
  WAITRESS_COMMAND='python {}'.format(PATHS['RUNSERVER_SCRIPT'])
  print_colored("Deployment successful!")
  print("""Now, open two command lines and execute the following commands in order to run the server:
  {}
  {}""".format(NGINX_COMMAND, WAITRESS_COMMAND))
except Exception as error:
  print_colored(error, colorama.Fore.RED)
